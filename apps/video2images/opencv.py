# OpenCV library
import cv2

# Python native libraries
import json, sys, pprint, math


def msg():
	print("please check the options, should look like this:\n\
	       -------------------------------------------------------\n\
	       python opencv2.py ./someVideoFile.mp4 --fps 3 --scale 5\n\
	       -------------------------------------------------------\n\
             ")

	sys.exit(1)

if len(sys.argv) == 6:

	# Check if the file name is empty
	if sys.argv[1] != None and sys.argv[1] != None: print(""); 
		#print("file name: ", sys.argv[1])

	else: msg()

	# Check if the fps is given and is not empty
	if sys.argv[2] == '--fps' and sys.argv[3] != None: print("")
		#print("frames per second: ", sys.argv[3])

	else: msg()

	# Check if the scale information is given and not empty
	if sys.argv[4] == '--scale' and sys.argv[5] != None: print("")
		#print("scale: ", sys.argv[5])
	else: msg()

# If all the 6 arguments are not given, then display the
# use the way to submit the command
else: msg()

# Populate the input json file
injs = { "uri":sys.argv[1], "fps":sys.argv[3], "scale":sys.argv[5] }
#print (injs)
	

# https://www.learnopencv.com/read-write-and-display-a-video-using-opencv-cpp-python/
# For capturing a video from path
cap = cv2.VideoCapture(sys.argv[1])

# output dictionary schema with input variables included
outjs = {"input":injs, "output":[]}

gArr = [] # global array for populating the json file
cc = 0 # file name counter as well as index
calc_timestamps = [0.0]

# frames per second
fps = cap.get(cv2.CAP_PROP_FPS)

# Start an infinite loop for processing all the frames in the given video file
while cap.isOpened():

	# Initialize the temp dictionary 
	p = {}

	# Populate values into the dictionary
	p["type"] = "Frame"
	p["index"] = cc

	ret, frame = cap.read()

	fInfo = './vfiles/frame' + str(cc) + '.jpg'
	cv2.imwrite(fInfo, frame)

	width = cap.get(3)
	height = cap.get(4)

	# Get time stamp of the frame
	timestamp = math.floor(cap.get(cv2.CAP_PROP_POS_MSEC)*1000)/1000

	# Code to resize the frame
	# Fetch the scaling size from the CLI for symmetric scaling of the image
	ss = float(sys.argv[5])
	frame_resize = cv2.resize(frame, (0, 0), fx=ss, fy=ss)

	# Populate values into the dictionary
	p["timestamp"] = timestamp
	p["snapshot"] = {"type":"Image", "uri":fInfo}
	p["width"] = width*float(ss)
	p["height"] = height*float(ss)
	gArr.append(p)

	# Append to the dictionary per iteration
	outjs["output"].append(p)

	# ----- UNIT TESTING -----
	# Enable this code to do unit testing
	# i.e. testing for a few frames only and 
	# exiting out of the loop, say 2 frames here
	if cc > 2:
		break

	cc += 1

# Convert the dictionary to a json file
ojs = json.dumps(outjs)

# print the output
print (ojs)
