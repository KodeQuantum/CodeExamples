# July 6, 2018
## Code for processing operations on video based on a set of inputs

`Dockerfile`: This contain the required information for initializing the *Docker Image* with required files pushed into the container
`opencv.py`: Is the main video processing code. To run the code, just execute `python opencv.py`, which will display the way to run the code and the required inputs. Takes a video files, something of the mp4 format. 

`stdout_dump`: A snapshot of what happens after running runMe.sh

Depends on OpenCV python libraries
https://docs.opencv.org/3.0-beta/doc/py_tutorials/py_tutorials.html



### Docker Container execution instructions 

chmod 700 runMe.sh
./runMe.sh


