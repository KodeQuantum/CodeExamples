#!/bin/bash

name=videoprocessing
cName=cont_${name}
iName=img_${name}

echo "--------------------------------------------------------------------------------------------------"
echo "Note that this script will remove the container named ${cName} and the image named ${iName}"
echo "and reinitialize with the same name, if you like to proceed, press [y] and then enter"
echo "--------------------------------------------------------------------------------------------------"


read p
if [[ $p == "y" ]]
then
	echo "Processing the code...."
else
	echo "You chose to quit...."
	exit 1
fi

# Stop and remove the container if already running
docker container stop ${cName}
docker container rm ${cName}
echo ; echo ;

# Delete the image, if already present
docker image rm -f ${iName}
echo ; echo ;

# Create the image and start the container
docker build -t ${iName} .
docker run -it --name ${cName} -d ${iName}
echo ; echo ;

echo "You've created the image as shown below"
docker image ls
echo ;

echo "The image is initialized into the container"
docker container ls
echo ;

echo "---------------------------------------------------------"
echo "you are now in the container named ${cName}"
echo "to exit out of the container, type [exit] and press enter"
echo "---------------------------------------------------------"
echo ;

echo "run the code as shown below in the command prompt inside the container"
echo "--------------------------------------------------------------------------"
echo "python opencv.py ./testVideo.mp4 --fps 3 --scale 0.5 | python -m json.tool"
echo "--------------------------------------------------------------------------"

# Get into the container
docker exec -it ${cName} /bin/bash
