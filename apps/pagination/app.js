
/*****************************************************************************
		SCROLL TO THE BOTTOM FOR INSTRUCTIONS
*****************************************************************************/

// Global Variables
const gPagination = 2


// Module export and initialization
var keypress = require('keypress');

var events = require('events');
var eventEmitter = new events.EventEmitter();

var MongoClient = require('mongodb').MongoClient;

// Provide your mongodb url here
// not this is unauthorized database server
var url = "mongodb://172.17.0.3:27017/";


var nxt = 0;

/* Read information from database */
function retVal(cursor, nxt, totObjs){

	console.log("Reading the ", nxt, " objects out of a total of ", totObjs);
	cursor.skip(nxt).limit(gPagination);
	cursor.forEach(function(item){
		console.log(item);
	});
 
};


/* Event handler to read information from database when a key is pressed */
var myEventH = function(nxt, vg){

	MongoClient.connect(url, function(err, db) {
	  if (err) throw err;
	  console.log("Database created!");
	
	  // Database name: mydb
	  var dbo = db.db("mydb");
	  myobj = [{"n1":1, "n2":2}];

	  // Collection name: mycoll
	  dbo.collection("mycoll").insertMany(myobj, function(err, res){
		if (err) throw err;
		console.log("No of documents inserted: ", res.insertedCount);
	  })


	  // Core logic is bound to the cursor object, and promise resolution for pagination
	  var cursor = dbo.collection("mycoll").find();
          var totObjs = 0;
	  cursor.count().then(function(value){console.log("number of objects: ", value); totObjs = value; }).finally(()=>{
		  retVal(cursor, nxt, totObjs);
		  db.close();
	  })
	});
}


/* Initialization function that listens to key presses, when this code is used to test on the CLI */
eventEmitter.on('dbEvent', myEventH);
function init(){

	keypress(process.stdin);
	 
	// listen for the "keypress" event
	process.stdin.on('keypress', function (ch, key) {
	  console.log('got "keypress"', key);
	  if (key && key.name == 'c') {
	    eventEmitter.emit('dbEvent', nxt, gPagination);
	    nxt = nxt + 2;
	  }

	  if (key && key.name == 'd') {
	    process.stdin.pause();
	  }

	});
	 
	process.stdin.setRawMode(true);
	process.stdin.resume();
}

init();


/* 
Algorithm: 
    Use a keyboard event to open the database and read k elements successively with each strike.

   NOTE: All operations are done on the server side

    Configuration:
	- MongoDB URL, and PORT
	- gPagination : provide an integer to read the maximum number per call

	- Start the server	
	node app.js


	Usage: 
	- Type 'c' without quotes to read the information from the database
	- Type 'd' to stop the node server
	
	At the stdout of the server, should see an output, that looks like this: 
	For testing purpose a dummy database is initialized, and also with each hit, writes some more information into the database

	Database created!
	No of documents inserted:  1
	number of objects:  1169
	Reading the  28  objects out of a total of  1169
	{ _id: 5ca9fd37095b78633d61cfa1,
	  msg0: 'm3',
	  msg1: 'm3',
	  msg2: 'm1',
	  msg3: 'm2',
	  msg4: 'm3',
	  msg5: 'm1',
	  msg6: 'm2' }
	{ _id: 5ca9fd38095b78633d61cfa2,
	  msg0: 'm1',
	  msg1: 'm3',
	  msg2: 'm3' }
	got "keypress" { name: 'd',
	  ctrl: false,
	  meta: false,
	  shift: false,
	  sequence: 'd' }


*/
