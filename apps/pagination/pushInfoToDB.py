import random as rng
import time
import requests
import json
import threading
import sys
import math

import pymongo.collection as pymColl
from pymongo import MongoClient


# A random message of certain finite length
def genMsg():

	# List of possible messages the cam can generate
	msg = {'e1':'m1', 'e2':'m2', 'e3':'m3'}
	lm = len(msg)

	
	bb = {}
	for e in range(0, rng.randint(1,9)):

		# Read from a random message
		mid = rng.randint(1,1000)%lm
		mk = msg[list(msg.keys())[mid]]
		
		# Write to an arbitrarily sized dictionary
		m = 'msg' + str(e)
		bb[m] = mk

	if bb != {}: return bb
	else: genMsg()



def pushValues():

	cs = 'mycoll'
	ip = '172.17.0.3' # Modify database URL here
	client = MongoClient(ip, 27017) # Modify port information here
	db = client['mydb']
	mycol = db[cs]
	db[cs].drop()

	while True:

		m = genMsg(); 
		st = mycol.insert_one(m)
		print(st, m)
	
		k = math.floor(rng.uniform(0,1)*1000)/1000
		time.sleep(k)


pushValues()
