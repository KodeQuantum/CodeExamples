#!"D:\Program Files\Python34\python.exe"


#print("Content-Type: text/html\n\n")
print("Content-Type: application/json\n\n")

import sys, json, requests

resultLocal = {"success":"true","message":"The Command Completed Successfully"}

result = json.JSONEncoder().encode(resultLocal)

from pymongo import MongoClient
import datetime

import mysql.connector

cnx = mysql.connector.connect(user='root', password='putItHere',
                              host='127.0.0.1',
                              database='sakila')
							  
cursor = cnx.cursor()

query = ("SELECT first_name, last_name, hire_date FROM employees "
         "WHERE hire_date BETWEEN %s AND %s")

hire_start = datetime.date(1999, 1, 1)
hire_end = datetime.date(1999, 12, 31)

cursor.execute(query, (hire_start, hire_end))

for (first_name, last_name, hire_date) in cursor:
  print("{}, {} was hired on {:%d %b %Y}".format(
    last_name, first_name, hire_date))

cursor.close()
cnx.close()


