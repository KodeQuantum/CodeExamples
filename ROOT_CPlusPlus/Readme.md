## Graphical User Interface (GUI) to streamline visualization and processing of results

#### [ROOT library](https://root.cern.ch/root/htmldoc/guides/users-guide/WritingGUI.html) has the basic GUI components to build an application on the Linux system. 

The code [main](https://gitlab.com/KodeQuantum/CodeExamples/tree/Dev/ROOT_CPlusPlus) is a single file that can process the visualizations and apply the desired cosmetics. 

Functionality :
* Take two root files as inputs, *fileA.root*, *fileB.root*, via the basic GUI widget.
* **Multi object display per ROOT file:** Various objects like 1D, 2D, 3D histograms, graphs, etc are shown with a check box against it.
* **Divide two histograms:** By checking two checkboxes for 1D histograms, say h1A:fileA.root and h1B:fileB.root, and click on Divide, will generate the ratio of h1A/h1B. The order of ticking matters here, if B is checked first and then A, then it would be h1B/h1A. 
* **Modify the marker attributes:** Click anywhere on the canvas on a particular marker, and a Dialog box shows up, asking you change the [color, size, type] of the marker
* **Save the result to a output file:** Type a file name and the result shown on the canvas would be saved into a separate file name

Since, any number of files can be opened, taking ratios would be very easy with this utility. No reprogramming is necessary for every change in the file.

