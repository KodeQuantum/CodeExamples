console.log("Inside the file events.js");


const EventEmitter = require('events');

console.log(this);

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

myEmitter.on('event', ()=>{
	console.log("an event occured");
});

myEmitter.emit("event");

myEmitter.emit('event', 'a', 'b');
