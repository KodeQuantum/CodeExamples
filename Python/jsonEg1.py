#from types import *
import json, types


dat = {"status": "ok", "mongo_query": {"fields": ["dataset", "das_id", "cache_id", "das", "qhash", "error", "reason"], "qcache": 0, "spec": {"dataset.name": "/PAHighMultiplicity1/PARun2016C-PromptReco-v1/AOD"}, "instance": "prod/global"}, "ctime": 0.612056016922, "nresults": 1, "timestamp": 1504661065.64, "incache": "true", "data": [{"das_id": ["59af4e0d4bf01c1b420cfce5", "59af4e0d4bf01c1b420cfce8", "59af4e0d4bf01c1b420cfce2"], "qhash": "69a3427d2e5d3d1e965cbd82d1a2c2e1", "cache_id": ["59af4e0f4bf01c1b420cfd19", "59af4e0d4bf01c1b420cfcf8", "59af4e0d4bf01c1b420cfd03"], "dataset": [{"status": "VALID", "modified_by": "tier0@vocms001.cern.ch", "physics_group_name": "NoGroup", "acquisition_era_name": "PARun2016C", "primary_dataset": {"name": "PAHighMultiplicity1"}, "creation_time": "2016-11-20 04:40:40", "data_tier_name": "AOD", "created_by": "tier0@vocms001.cern.ch", "processed_ds_name": "PARun2016C-PromptReco-v1", "modification_time": "2016-11-20 04:40:40", "datatype": "data", "xtcrosssection": "null", "processing_version": 1, "prep_id": "null", "dataset_id": 13289150, "name": "/PAHighMultiplicity1/PARun2016C-PromptReco-v1/AOD"}, {"status": "VALID", "modified_by": "tier0@vocms001.cern.ch", "physics_group_name": "NoGroup", "acquisition_era_name": "PARun2016C", "primary_dataset": {"name": "PAHighMultiplicity1"}, "creation_time": "2016-11-20 04:40:40", "data_tier_name": "AOD", "created_by": "tier0@vocms001.cern.ch", "processed_ds_name": "PARun2016C-PromptReco-v1", "modification_time": "2016-11-20 04:40:40", "datatype": "data", "xtcrosssection": "null", "processing_version": 1, "prep_id": "null", "dataset_id": 13289150, "name": "/PAHighMultiplicity1/PARun2016C-PromptReco-v1/AOD"}, {"name": "/PAHighMultiplicity1/PARun2016C-PromptReco-v1/AOD", "nevents": 145517974, "nlumis": 22423, "nfiles": 6430, "nblocks": 59, "size": 20889309433426}], "das": {"primary_key": "dataset.name", "record": 1, "condition_keys": ["dataset.name"], "ts": 1504661008.52982, "system": ["dbs3", "dbs3", "dbs3"], "instance": "prod/global", "api": ["datasets", "dataset_info", "filesummaries"], "expire": 1504661305, "services": [{"dbs3": ["dbs3"]}, {"dbs3": ["dbs3"]}, {"dbs3": ["dbs3"]}]}, "_id": "59af4e104bf01c1b420cfd24"}], "apilist": ["das_core", "datasets", "filesummaries", "dataset_info"]}


def recFn(jdat):
    print len(jdat.keys())
    if(len(jdat.keys())):
        for jd in jdat.keys():
            #$recFn(jd)
            #print type(jdat[jd])
            if type(jdat[jd]) is types.DictType:
            #if (type(jdat))
                recFn(jdat[jd])
                print "\n"

            elif type(jdat[jd]) is types.ListType:
                print "list"

            elif type(jdat[jd]) is types.StringType:
                print "string"

            elif type(jdat[jd]) is types.FloatType:
                print "float"
        


recFn(dat)

