
''' Written for Python 2.7.9 '''
#!/bin/python

# https://docs.python.org/2/library/types.html : Type of object, like an array, string, boolean, etc 

import json

class Animal:

    def __init__(self, inFile):

        
        self.inputFile = open(inFile, "r")
        print self.inputFile

        #param = json.dumps(json.load(self.inputFile))

        param = json.load(open(inFile, "r"))

        # print type(json.load(self.inputFile))
        print param
        print type(param)

        # Loop over the objects serially
        for dk, dv in param.iteritems():

            # dk will be list of keys matched
            # Loop over the desired parameters and 
            # programmatically assign the attributes

            if dk == "a1":
                print "found the match: ", dk, "\t", dv

                # Assign the values this way by 
                # looping over the attributes and 
                # assigning them 
                self.a1 = dv

            # Use sys.exit() if necessary to exit out the program
            # or a try, except block

    def algo1(self):
        print "inside the algo1"
        # do something here with the parameters

    def algo2(self):
        print "inside the algo2"
        # do something here with the parameters


    # Check the size of the json i.e
    # the class gets the desired info
    # Loop over the json file



# Unit test of class
a = Animal("test.json")
