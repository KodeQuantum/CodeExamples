`model.ipynb`: CNN+LSTM network, based on the hybrid architecture for feature extraction followed by sequence training. 

Limited number of layers are set up, as it is computationally very expensive to run both CNN, and LSTM together. 

**plots**: folder contains outputs from `model.ipynb`. The input datasets are filtered for the year 2014 only. The results are for just 16 date pairs, i.e. to and from for 100 epochs

output\_\*.jpg : contains the results in the folder, and loss\_\*.jpg : are the loss plots which are progressively reducing at higher epochs. This is a good sign that the network training is stable. 

`Dataset`: divvy, which can be obtained from kaggle. 

`model-2.ipynb`: Is a different architecture, relatively simpler to understand the loss changes with epochs. 

